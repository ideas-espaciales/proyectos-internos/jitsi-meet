#!/bin/bash

credentials=()
# syntax:
# "username meet.jitsi password"
credentials[0]="hello meet.jitsi 1234"

for i in "${credentials[@]}"
do
    docker exec -it jitsi_prosody prosodyctl --config /config/prosody.cfg.lua register ${i}
done